---
title: 'Bug'
description: 'Frontmatter parsing bug'
date: '2022-07-04'
---

# Frontmatter Parsing Bug

Check the browser tab when in dev mode and the title tag in the inspector.

The title is showing as

`'Bug'\ndescription: 'Frontmatter parsing bug'\ndate: '2022-07-04'`